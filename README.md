
# PiperCI Python Libraries

A python library for interacting with the various components of PiperCI

[![Build Status](https://gitlab.com/dreamer-labs/piperci/python-piperci/badges/master/pipeline.svg)](https://gitlab.com/dreamer-labs/piperci/python-piperci)

[Project Source](https://gitlab.com/dreamer-labs/piperci/python-piperci)

[API Docs](https://piperci.dreamer-labs.net/python-piperci/README)

## Installation

`pip install piperci`

OR

`pip install -e piperci@git+https://gitlab.com/dreamer-labs/piperci/python-piperci.git`

## Provided Libraries

- faas.ThisTask: a wrapper class to ease the burden of developing PiperCI compatible FaaS functions.
- ArtMan Client: a client library for interacting with artifact REST APIs
- GMan Client: a client library for interacting with the gman REST APIs
- StoreMan: a library for interacting with the storage backends
- sritool: a CLI and python utility library for generating sri hashes used for working with ArtMan

## Contributing

[Contributing to PiperCI](https://piperci.dreamer-labs.net/project-info/contributing)

## License

[MIT](https://piperci.dreamer-labs.net/project-info/LICENSE)
