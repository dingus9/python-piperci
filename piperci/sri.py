import base64
import hashlib
import os

import subresource_integrity as integrity


def hash_to_urlsafeb64(hash):
    """Convert an integrity.Hash object to urlsafe encoded sri hash.
    :param integrity.Hash: An integrity.Hash object
    :return: string urlsafe encoded SRI
    """

    assert isinstance(hash, integrity.Hash), 'to_urlsafe encodes a Hash to urlsafe b64'
    return base64.urlsafe_b64encode(str(hash).encode('ascii')).decode('utf-8')


def urlsafe_to_hash(url_safe_b64):
    """Convert a urlsafe_b64encode'd string to and integrity.Hash object.
    :param url_safe_b64: A urlsafe_b64encoded bytes or string object
    :return: integrity.Hash object
    """

    assert isinstance(url_safe_b64, str) or isinstance(url_safe_b64, bytes), (
        'decodes urlsafe "string" or "bytes" b64 to a Hash')

    if isinstance(url_safe_b64, str):
        url_safe_b64 = url_safe_b64.encode('ascii')

    url_safe_b64 = base64.urlsafe_b64decode(url_safe_b64).decode('utf-8')
    return integrity.parse(url_safe_b64)[0]


def sri_to_hash(sri):
    """Convert an SRI encoded string to integrity.Hash.
    :param sri: A string sri like digest-hash_value
    :return: integrity.Hash object
    """
    return integrity.parse(sri)[0]


def hash_to_sri_hash(sri_dgst, sri_bytest_value):
    """Coverts a b64 encoded binary digest and value to integrity.Hash boject
    :param sri_dgst: The digest hash method ex: sha256, md5, sha512
    :param sri_bytest_value: The SRI bytes hash value.
    :return: integrity.Hash object
    """

    if isinstance(sri_bytest_value, bytes):
        return integrity.Hash(sri_dgst, sri_bytest_value)
    else:
        raise ValueError('hash value is not a bytes or str can\'t encode')


def hash_file(sri_dgst, path):
    """Generate a digest hash from file path.
    :param sri_dgst: string digest hash method ex: sha256, md5, sha512
    :param path: string path to the file to generate an SRI.
    """
    h = hashlib.new(sri_dgst)

    with open(path, 'rb') as file:
        for chunk in iter(lambda: file.read(4096), b""):
            h.update(chunk)
    return h.digest()


def b64_hash(bhash):
    """Converts a raw binary hash object to b64encoded hash for manual sri generation.
    :param bhash: raw binary hash object to b64encode
    :return: base64 encoded string of bhash
    """
    assert isinstance(bhash, bytes)
    return base64.b64encode(bhash)


def generate_sri(path, dgst='sha256', url_safe=False):
    """Generate an SRI from a file path.
    :param path: string path to the file to generate an SRI.
    :param sri_dgst: string digest hash method ex: sha256, md5, sha512
    :param url_safe: boolean flag to encode as a urlsafe base64 string
    :return string SRI hash or urlsafe SRI hash
    """
    path = os.path.realpath(path)
    bhash = hash_file(dgst, path)
    hash = hash_to_sri_hash(dgst, bhash)
    return hash_to_urlsafeb64(hash) if url_safe else hash
