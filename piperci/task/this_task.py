import io
import logging
import os
import tarfile
import tempfile
import zipfile

import requests

from urllib.parse import urlparse

from pythonjsonlogger import jsonlogger

from piperci.task.exceptions import (
    PiperActiveError,
    PiperDelegateError,
    PiperLoggingError,
    PiperError,
)
from piperci.artman import artman_client, exceptions as artman_exceptions
from piperci.gman import client as gman_client
from piperci.sri import generate_sri, sri_to_hash, hash_to_urlsafeb64
from piperci.storeman.client import storage_client
from piperci.storeman.exceptions import StoremanError


def init_logger(log_level="info", log_file="piperci_task.log"):
    """
    Initializes the Python logger
    :param log_level: Sets the log level for all handlers
    :param log_file: Sets the name of the logger and the file that we log to
    :return: Reference to the logger
    """
    logger = logging.getLogger(log_file)
    if not len(logger.handlers):
        logger.setLevel(log_level.upper())

        stream = logging.StreamHandler()
        stream.setLevel(log_level.upper())
        formatter = jsonlogger.JsonFormatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s", timestamp=True
        )
        stream.setFormatter(formatter)

        file = logging.FileHandler(log_file)
        file.setLevel(log_level.upper())
        formatter = jsonlogger.JsonFormatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s", timestamp=True
        )
        file.setFormatter(formatter)

        logger.addHandler(stream)
        logger.addHandler(file)

    return logger


class ThisTask:
    def __init__(
        self,
        gman_url,
        *,
        storage,
        project,
        thread_id=None,
        parent_id,
        run_id,
        status,
        stage,
        caller,
        log_file="piperci_task.log",
    ):
        """
        Representation of a task object from GMan.
        :param gman_url: URL for GMan
        :param storage: Dict containing storage parameters
        :param project: Project name
        :param thread_id: Thread to associate this task with
        :param parent_id: Parent task ID to associate this task with
        :param run_id: Run to associate this task with
        :param status: Status of the task. Must be "started" or "received"
        :param stage: The name of the stage that this task is associated with.
        :param caller: The name of the program that created this task.
        :param log_file: The log file path.
        """

        if run_id:
            run_id = str(run_id)
        else:
            raise PiperActiveError("run_id must not be None")

        self.init_params = {"gman_url": gman_url,
                            "storage": storage,
                            "project": project,
                            "thread_id": thread_id,
                            "parent_id": parent_id,
                            "run_id": run_id,
                            "status": status,
                            "stage": stage,
                            "caller": caller,
                            "log_file": log_file
                            }

        self.logger = init_logger(log_file=self.log_file)
        self.storage_client = self._init_storage_client(**storage)
        self.task = self._start(
            project=project,
            thread_id=thread_id,
            parent_id=parent_id,
            run_id=run_id,
            status=status,
            caller=caller,
        )
        self._set_artifact_types()
        self._unique_file = 0

    def _set_artifact_types(self):
        """
        Dynamically binds methods to ThisTask based on the allowed
        types of artifacts from artman_client.
        Example: If the allowed artifact_types are ['stdout', 'stderr']
        then we will create and bind a `stdout` and `stderr` method.
        :return: None
        """
        artifact_types = artman_client.artifact_types()
        for artifact_type in artifact_types:

            def func(self, content, filetype=artifact_type, name=None, is_path=False):
                try:
                    return self._upload(content=content,
                                        filetype=filetype,
                                        upload_as=name,
                                        is_path=is_path)
                except Exception:
                    self.info(f"failed to upload {artifact_type}")
                    raise PiperLoggingError(f"failed to upload {artifact_type}")

            setattr(self, artifact_type, func.__get__(self, self.__class__))

    def _init_storage_client(self, storage_type, **kwargs):
        """
        Initializes the storage client.
        :param storage_type: The type of storage that will be used
        :param kwargs: Required KWargs depend on the storage type.
          For Minio:
          hostname: The hostname of the storage service.
          access_key: The access key for the storage service.
          secret_key: The secret key for the storage service
        :return: piperci.storeman.client.BaseStorageClient
        """

        minio_client = storage_client(
            storage_type=storage_type,
            hostname=kwargs.get("hostname"),
            access_key=kwargs.get("access_key"),
            secret_key=kwargs.get("secret_key"),
            secure=False,
        )
        return minio_client

    def _start(self, *, project, thread_id, parent_id, run_id, status, caller):
        """
        Requests a task from GMan
        :param project: The project name that the task is associated with.
        :param thread_id: The thread that the task is associated with. Can be none.
        :param parent_id: The parent task ID.
        :param status: The status to initialize the task with.
        Must be "received" or "started"
        :param caller: The caller of the task.
        :return: Task object from GMan
        """
        self.logger.info(f"Requesting task from GMan")

        data = {
            "gman_url": self.gman_url,
            "run_id": run_id,
            "parent_id": parent_id,
            "project": project,
            "thread_id": thread_id,
            "status": status,
            "caller": caller,
        }
        self.logger.debug(f"Params {data}")
        try:
            task = gman_client.request_new_task_id(**data)
            self.logger.info(f"Received new task {task}")
            return task
        except requests.RequestException as e:
            self.logger.error(
                f"Encountered unknown requests exception while trying to "
                f"request new task. {e}"
            )
            raise PiperActiveError(e)
        except ValueError as e:
            self.logger.error(
                f"ValueError received while trying to request a new task. {e}"
            )
            raise PiperActiveError(e)

    def _upload(self, *, content, filetype="log",
                upload_as=None, is_path=False):
        """
        Uploads a Filelike/string/file_path/folder to Storage a storage backend.
        :param content: File like, string, or path to be uploaded
        :param filetype: The type of the file. "log" or "artifact"
        :param upload_as: The destination name of the file,
                          if not specified one will be generated.
        :param is_path: bool the content is a valid path not open file or string
        :return:
        """
        if is_path:
            upload_as = self.gen_file_name(basename=os.path.basename(content))
            if os.path.isdir(content):
                artifacts = []
                for root, dir, files in os.walk(content):
                    for file in files:
                        artifacts.append(self._upload_file(file,
                                                           filetype=filetype,
                                                           name=upload_as))
                return artifacts
            elif os.path.isfile(content):
                return self._upload_file(content, filetype=filetype, name=upload_as)
            else:
                self.info(f"Invalid path: {content}")
                raise PiperLoggingError(f"Invalid path: {content}")
        else:
            upload_as = self.gen_file_name(basename=upload_as)
            with tempfile.NamedTemporaryFile() as temp:
                if isinstance(content, str):
                    content = io.BytesIO(content.encode())

                try:
                    temp.write(content.read())
                except IOError as e:
                    raise PiperLoggingError(f"Error writing raw file to {temp.name}:"
                                            f"{str(e)}")

                return self._upload_file(temp.name, filetype=filetype, name=upload_as)

    def _upload_file(self, path, *, filetype, name=None):
        """
        Uploads a file to the storage backend and tells ArtMan that a file
        has been uploaded with a given URI
        :param file: The path to the file that is being uploaded
        :param filetype: The type of the file. "log" or "artifact"
        :return: None
        """
        if not name:
            name = path

        try:
            self.logger.info(
                f"Uploading file {path} as {name} to"
                f"{self.get_artifact_uri(name, filetype)}"
            )
            bucket, object_name = self.get_artifact_uri(name, filetype, False)
            self.storage_client.upload_file(bucket, object_name, path)

            self.info(f"Uploaded file {path} as {name}")
            project_artifact_hash = generate_sri(path)
            artifact_uri = self.get_artifact_uri(name, filetype)
            artifact = artman_client.post_artifact(
                task_id=self.task_id,
                artman_url=self.gman_url,
                uri=artifact_uri,
                caller=self.caller,
                type=filetype,
                sri=str(project_artifact_hash),
            )
            self.logger.info(f"Artifact uploaded at {artifact_uri}")
            return artifact
        except StoremanError as e:
            self.logger.error(
                f"Encountered unknown exception while trying to upload files to storage."
                f"{e}"
            )
            raise PiperLoggingError(e)
        except artman_exceptions.ArtmanRequestException as e:
            message = (
                f"Encountered unknown requests exception while trying to"
                f"post artifact to ArtMan. {e}"
            )
            self.logger.error(message)
            raise PiperLoggingError(e)

    @property
    def short_id(self):
        return self.task_id[:8]

    def complete(self, message, return_code=None):
        """
        Completes the task with a given message
        :param message: Message to send to GMan on task completion.
        :return: A reference to the task and 200
        """
        self.logger.info(message)
        try:
            task_id = self.task_id
            self._upload_file(self.log_file, filetype="log")
            gman_update_args = {
                "task_id": task_id,
                "gman_url": self.gman_url,
                "status": "completed",
                "message": message
            }
            if return_code is not None:
                gman_update_args.update({"return_code": return_code})
            gman_client.update_task_id(**gman_update_args)
            return self.task, 200
        except requests.exceptions.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when trying to complete task. {e}"
            )
            raise PiperLoggingError(e)

    def delegate(self, url, data):
        """
        Attempts to delegate a task to another PiperCI Task, usually an executor
        Raises PiperDelegateError if this fails
        Marks a task as delegated
        :return: None
        """
        self.info(f"Attempting to delegate execution to {url}")
        data["stage"] = self.stage
        data.update(self.task["task"])
        data["parent_id"] = self.task_id
        data["thread_id"] = self.task["task"]["thread_id"]
        try:
            r = requests.post(url, json=data)
            r.raise_for_status()
        except requests.exceptions.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when trying to delegate task. {e}"
            )
            raise PiperDelegateError(e)

        try:
            gman_client.update_task_id(
                task_id=self.task_id,
                gman_url=self.gman_url,
                status="delegated",
                message=f"delegated execution to {url}",
            )
        except requests.exceptions.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when updating task"
            )
            raise PiperDelegateError(e)

        self.info(f"Successfully delegated task {self.task} to {url}.")

    def fail(self, message=None, return_code=None, http_status=400):
        """
        Marks a task as failed in GMan and sends 400 to client
        :param message: None a message to pass to the logger and GMan
        :param return_code: None a command or process return code value
        :http_status: 400 an http_status code to be returned
        :return: tuple obj, http_status
        """
        self.logger.error(message)
        try:
            self._upload_file(self.log_file, filetype="log")
            gman_update_args = {
                "gman_url": self.gman_url,
                "status": "failed",
                "task_id": self.task_id,
                "message": message,
            }
            if return_code:
                gman_update_args.update({"return_code": return_code})
            gman_client.update_task_id(**gman_update_args)
        except requests.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when trying to mark task failed."
                f"{e}"
            )
            raise PiperLoggingError(e)
        return {"errors": {"task.fail": [message]}}, http_status

    def get_source(self, *, artifact, extract_to=None):  # noqa: C901
        """
        Queries ArtMan and downloads a source artifact from the storage backend.
        :artifact dict or str(sri): of the artifact to download as a artifact dict or
            string sri.
        :extract_to str(filename): The filename to extract the source to,
            if None, default to name of artifact.
        :return dict artifact: With an added extract_to destination path
            of the extracted artifact.
        """
        self.logger.info("Querying ArtMan for source artifact")

        if isinstance(artifact, str):
            try:
                sri_urlsafe = hash_to_urlsafeb64(sri_to_hash(artifact))
                source_artifact = next(
                    iter(artman_client.get_artifact(artman_url=self.gman_url,
                                                    sri_urlsafe=sri_urlsafe)),
                    None,
                )
            except Exception:
                raise PiperError("Unable to convert artifact sri to a urlsafe sri")
        elif "uri" in artifact:
            source_artifact = artifact

        else:
            raise PiperError("Not an PiperCI artifact dict")

        if not source_artifact:
            raise PiperError("No Artifact returned from the Artifact manager")

        if extract_to:
            extract_to = os.path.join(
                os.path.realpath(os.curdir),
                extract_to
            )
        else:
            extract_to = os.path.join(
                os.path.realpath(os.curdir),
                os.path.basename(urlparse(source_artifact.get("uri")).path)
            )

        source_artifact["extract_to"] = extract_to

        self.info(f"Detected extract_to path as {extract_to}")

        self.info(
            f"Downloading source artifact {source_artifact.get('uri')} from storage",
            artifact_id=source_artifact.get("artifact_id"),
        )
        try:
            with tempfile.TemporaryDirectory() as tmpdir:
                tmp_download_file = os.path.join(
                    tmpdir,
                    os.path.basename(extract_to)
                )
                self.storage_client.download_file(
                    source_artifact.get("uri"), tmp_download_file
                )

                if zipfile.is_zipfile(tmp_download_file):
                    self.logger.info(
                        "Detected source artifact was a zipfile. "
                        f"Unzipping to {extract_to}"
                    )
                    with zipfile.ZipFile(tmp_download_file, "r") as zip_ref:
                        zip_ref.extractall(extract_to)
                elif tarfile.is_tarfile(tmp_download_file):
                    self.logger.info(
                        "Detected source artifact was a tarfile. "
                        f"Extract to {extract_to}"
                    )
                    with tarfile.open(tmp_download_file) as tar_ref:
                        tar_ref.extractall(extract_to)

            return source_artifact
        except Exception as e:
            raise PiperError(f"Failed to extract {extract_to} because {str(e)}")

    def info(self, message, artifact_id=None, return_code=None):
        """
        Logs an info message to GMan
        :param message: Message to log
        :param artifact_id: Artifact to associate
        :return:
        """
        self.logger.info(message)
        gman_update_args = {
            "gman_url": self.gman_url,
            "status": "info",
            "task_id": self.task_id,
            "message": message
        }
        if artifact_id:
            gman_update_args.update({"artifact_id": artifact_id})
        if return_code:
            gman_update_args.update({"return_code": return_code})
        try:
            gman_client.update_task_id(**gman_update_args)
        except requests.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception while trying to log to GMan. {e}"
            )
            raise PiperLoggingError(e)

    def get_artifact_uri(self, file, filetype, fqdn=True):
        hyphenate_bad_chars = str.maketrans("_ ", "--")
        container = f"run-{self.run_id.translate(hyphenate_bad_chars)}"
        object_name = f"{filetype}/{self.stage}/{file}"
        if fqdn:
            return self.storage_client.gen_file_uri(container, object_name)
        else:
            return (container, object_name)

    def gen_file_name(self, basename=''):
        if not basename:
            self._unique_file += 1
            basename = f"{self._unique_file}-out"
        return f"{self.short_id}-{basename}"

    @property
    def caller(self):
        try:
            return self.task["task"]["caller"]
        except KeyError:
            return self.init_params["caller"]

    @property
    def project(self):
        try:
            return self.task["task"]["project"]
        except KeyError:
            return self.init_params["project"]

    @property
    def thread_id(self):
        try:
            return self.task["task"]["thread_id"]
        except KeyError:
            return self.init_params["thread_id"]

    @property
    def task_id(self):
        return self.task["task"]["task_id"]

    @property
    def parent_id(self):
        try:
            return self.task["task"]["parent_id"]
        except KeyError:
            return self.init_params["parent_id"]

    @property
    def run_id(self):
        try:
            return self.task["task"]["run_id"]
        except KeyError:
            return self.init_params["run_id"]

    @property
    def status(self):
        return self.init_params["status"]

    @property
    def stage(self):
        return self.init_params["stage"]

    @property
    def log_file(self):
        return self.init_params["log_file"]

    @property
    def gman_url(self):
        return self.init_params["gman_url"]

    @property
    def storage(self):
        return self.init_params["storage"]
